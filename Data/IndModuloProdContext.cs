﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using AprendendoCore.Models;

namespace AprendendoCore.Data
{
    public class IndModuloProdContext : IdentityDbContext
    {
        public IndModuloProdContext(DbContextOptions<IndModuloProdContext> options)
        : base(options)
        {
        }
        public DbSet<Indicador> Indicador { get; set; }
        public DbSet<PlanoAcao> PlanoAcao { get; set; }
    }
}
