﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AprendendoCore.Migrations
{
    public partial class IncluirTabelaPlanoAcao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PlanoAcao",
                columns: table => new
                {
                    PlanoAcaoId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UsuarioId = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    IndicadorId = table.Column<int>(nullable: false),
                    Fato = table.Column<string>(nullable: true),
                    Causa = table.Column<string>(nullable: true),
                    Acao = table.Column<string>(nullable: true),
                    Prazo = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanoAcao", x => x.PlanoAcaoId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlanoAcao");
        }
    }
}
