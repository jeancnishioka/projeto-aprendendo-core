﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AprendendoCore.Migrations
{
    public partial class alteracaoTabelaIndicador : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UsuarioId",
                table: "Indicador",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UsuarioId",
                table: "Indicador");
        }
    }
}
