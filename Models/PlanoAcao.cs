using System;

namespace AprendendoCore.Models
{
    public class PlanoAcao
    {
        public int PlanoAcaoId { get; set;}
        public int UsuarioId {get; set;}
        public DateTime? CreatedAt  { get; set; }
        public int IndicadorId { get; set; }
        public string Fato { get; set; }
        public string Causa { get; set; }
        public string Acao { get; set; }
        public DateTime Prazo { get; set; }
    }
}