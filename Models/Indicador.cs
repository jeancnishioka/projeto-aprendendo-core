using System;

namespace AprendendoCore.Models
{
    public class Indicador
    {
        public int IndicadorId { get; set;}
        public string Descricao { get; set; }
        public int UsuarioId {get; set;}
        public DateTime? CreatedAt  { get; set; }
    }
}